import java.util.Arrays;

public class Question2 {
    public static void main(String[] args) {
        
        int[] playerOne = {1, 4, 7, 2, 4};
        int[] playerTwo = {3, 4, 2, 4, 4};
        
        int scorePlayerOne = 0, scorePlayerTwo = 0;
        
        
        for (int i = 0; i <= playerOne.length - 1; i++) {
            if (playerOne[i] < playerTwo[i]) {
                scorePlayerTwo++;
            } else if (playerTwo[i] < playerOne[i]) {
                scorePlayerOne++;
            }
        }
        
        int[] results = {1, 1};
        results[0] = scorePlayerOne;
        results[1] = scorePlayerTwo;
        
        System.out.println(Arrays.toString(results));
        if (scorePlayerOne < scorePlayerTwo) {
            System.out.println("Player Two wins");
        } else if (scorePlayerTwo < scorePlayerOne) {
            System.out.println("Player One wins");
        } else {
            System.out.println("Draw");
        }
    }
}